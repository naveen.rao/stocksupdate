package com.stocksupdate.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.stocksupdate.R
import com.stocksupdate.databinding.LayoutStockDataBinding
import com.stocksupdate.models.Stock

class StockRecyclerAdapter(
    private val values: MutableList<Stock>
) : RecyclerView.Adapter<StockRecyclerAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = DataBindingUtil.inflate<LayoutStockDataBinding>(
            LayoutInflater.from(parent.context),
            R.layout.layout_stock_data,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.view.data = values[position]
    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(val view: LayoutStockDataBinding) : RecyclerView.ViewHolder(view.root)
}