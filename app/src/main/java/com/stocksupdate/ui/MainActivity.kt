package com.stocksupdate.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.stocksupdate.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}