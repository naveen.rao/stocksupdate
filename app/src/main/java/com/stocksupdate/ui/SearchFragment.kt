package com.stocksupdate.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.stocksupdate.R
import com.stocksupdate.databinding.FragmentSearchBinding
import com.stocksupdate.models.StockViewModel
import com.stocksupdate.utils.ApiStatus


class SearchFragment : Fragment() {

    private lateinit var binding: FragmentSearchBinding

    private val viewModel: StockViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.apiResponse.observe(this,
            {
                when (it) {
                    is ApiStatus.Success -> {
                        binding.search.isEnabled = true
                        activity?.title = "Result for : ${binding.tickerSymbol.text}"
                        findNavController().navigate(R.id.action_searchFragment_to_stockPricesFragment)
                    }
                    is ApiStatus.Loading -> {
                        binding.search.isEnabled = false
                    }
                    is ApiStatus.Failure -> {
                        binding.search.isEnabled = true
                        Toast.makeText(
                            requireContext(),
                            it.errorMessage,
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }

            })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_search, container, false)

        binding.search.setOnClickListener {
            viewModel.searchStockData(binding.tickerSymbol.text.toString())
        }

        return binding.root
    }

    override fun onResume() {
        super.onResume()
        activity?.title = getString(R.string.app_name)
    }
}