package com.stocksupdate.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.stocksupdate.R
import com.stocksupdate.databinding.FragmentStockPricesListBinding
import com.stocksupdate.models.StockViewModel
import com.stocksupdate.utils.ApiStatus
import com.stocksupdate.utils.Utils.Companion.getDateFromTimeStamp
import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.ValueFormatter

class StockPricesFragment : Fragment() {

    private val viewModel: StockViewModel by activityViewModels()

    private lateinit var binding: FragmentStockPricesListBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_stock_prices_list, container, false)

        setupChart()
        // Set the adapter
        binding.list.adapter =
            StockRecyclerAdapter((viewModel.apiResponse.value as ApiStatus.Success).stockData)
        return binding.root
    }

    private fun setupChart() {

        val entries = (viewModel.apiResponse.value as ApiStatus.Success).stockData.map {
            Entry(it.timeStamp.toFloat(), it.stockValue.toFloat())
        }

        val lineDataSet = LineDataSet(entries, getString(R.string.stock_data))
        lineDataSet.setDrawValues(false)

        val lineData = LineData(lineDataSet)
        binding.chart.xAxis.valueFormatter = object : ValueFormatter() {
            override fun getAxisLabel(value: Float, axis: AxisBase?): String {
                return getDateFromTimeStamp(value.toLong())
            }
        }

        binding.chart.data = lineData
    }


}