package com.stocksupdate.utils

sealed class ApiStatus<out T> {
    object Loading : ApiStatus<Nothing>()
    data class Success<out T>(val stockData: T) : ApiStatus<T>()
    data class Failure(val errorMessage: String) : ApiStatus<Nothing>()
}
