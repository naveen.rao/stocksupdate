package com.stocksupdate.utils

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class Utils {

    companion object {
        fun getDateFromTimeStamp(timeStamp: Long) =
            SimpleDateFormat.getDateTimeInstance(
                DateFormat.LONG,
                DateFormat.SHORT
            ).format(Date(timeStamp * 1000))

    }
}
