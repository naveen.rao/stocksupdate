package com.stocksupdate.networking

import com.stocksupdate.models.StockApiResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("candle?resolution=1&from=1615298999&to=1615302599&token=c15h2g748v6tvr5kk9gg")
    fun getStockData(@Query("symbol") symbol: String): Call<StockApiResponse>


}