package com.stocksupdate.networking

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitUtils {
    private val baseUrl = "https://finnhub.io/api/v1/stock/"

    private val retrofit = Retrofit.Builder()
        .baseUrl(baseUrl)
        .addConverterFactory(GsonConverterFactory.create())
        .client(OkHttpClient.Builder().build())
        .build()

    fun getApiService(): ApiService {
        return retrofit.create(ApiService::class.java)
    }

    companion object {
        const val RESPONSE_OK = "ok"
    }
}