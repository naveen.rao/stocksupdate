package com.stocksupdate.models

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.stocksupdate.networking.RetrofitUtils
import com.stocksupdate.networking.RetrofitUtils.Companion.RESPONSE_OK
import com.stocksupdate.utils.ApiStatus
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class StockViewModel : ViewModel() {

    private lateinit var stockLiveData: MutableList<Stock>
    val apiResponse = MutableLiveData<ApiStatus<MutableList<Stock>>>()

    fun searchStockData(symbol: String) {
        if (symbol.isBlank()){
            apiResponse.postValue(ApiStatus.Failure("Please enter symbol"))
            return
        }
        apiResponse.postValue(ApiStatus.Loading)
        RetrofitUtils().getApiService().getStockData(symbol)
            .enqueue(object : Callback<StockApiResponse> {
                override fun onResponse(
                    call: Call<StockApiResponse>,
                    response: Response<StockApiResponse>
                ) {
                    if (response.isSuccessful && response.body()?.s == RESPONSE_OK) {
                        stockLiveData = response.body()?.c?.mapIndexed { index, d ->
                            Stock(
                                d,
                                response.body()?.t!![index]
                            )
                        }?.toMutableList()!!

                        apiResponse.postValue(ApiStatus.Success(stockLiveData))
                    } else if (response.body()?.s != RESPONSE_OK) {
                        apiResponse.postValue(ApiStatus.Failure("No Data found"))
                    }else
                        apiResponse.postValue(ApiStatus.Failure("Invalid Data Returned"))
                }

                override fun onFailure(call: Call<StockApiResponse>, t: Throwable) {
                    apiResponse.postValue(ApiStatus.Failure(t.localizedMessage!!))
                }
            })
    }
}