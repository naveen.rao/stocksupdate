package com.stocksupdate.models

import com.stocksupdate.utils.Utils.Companion.getDateFromTimeStamp

class Stock(val stockValue: Double, val timeStamp: Long) {
    fun getDate(): String {
        return getDateFromTimeStamp(timeStamp)
    }

}